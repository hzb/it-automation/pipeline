# Define the stages of the pipeline
stages:
  - validate  # First stage for validating configuration files
  - build     # Second stage for building and deploying using Ansible

# Job for validating configuration files
validate_config:
  image: python:3.10  # Use Python 3.10 Docker image for this job
  stage: validate     # This job belongs to the 'validate' stage
  before_script:
    - apt-get update && apt-get install -y git  # Update package lists and install Git
    - pip install jsonschema PyYAML  # Install required Python libraries for validation
  script:
    - git clone https://codebase.helmholtz.cloud/hzb/it-automation/ioc-config-validator  # Clone the validator repository
    - cd ioc-config-validator  # Change directory to the cloned repository
    - python3 validator.py  # Run the validation script
  rules:
    - changes:
        - ioc_config.yml  # Only run this job if ioc_config.yml has changed

# Job for deploying using Ansible
deploy_ansible:
  services:
    - docker:20.10.24-dind  # Use Docker-in-Docker service to run Docker commands
  needs: []  # This job does not depend on any previous jobs
  image: docker:20.10.24  # Use Docker 20.10.24 image for this job
  stage: build  # This job belongs to the 'build' stage
  variables:
    ANSIBLE_HOST_KEY_CHECKING: 'false'  # Disable host key checking for Ansible
    DOCKER_TLS_CERTDIR: "/certs"  # Set Docker TLS cert directory
  script:
    - apk update && apk add ansible git py3-pip  # Update package lists and install Ansible, Git, and pip
    - pip install --upgrade pip  # Upgrade pip to the latest version
    - pip install cookiecutter
    - pip install python-gitlab
    - pip install docker
    - pip install --upgrade docker
    - pip install ansible
    - ansible-galaxy collection install community.docker  # Install Ansible community Docker collection
    - rm /tmp/beamlines-ansible -rf  # Remove any existing clone of the repository
    - git clone --branch develop https://$CI_USER:$CI_TOKEN@codebase.helmholtz.cloud/hzb/beamlines-ansible /tmp/beamlines-ansible  # Clone the repository with Ansible playbooks
    - cd /tmp/beamlines-ansible  # Change directory to the cloned repository
    - export ANSIBLE_VAULT_PASSWORD_FILE=~/.vault_pass  # Set the Ansible Vault password file location
    - echo $ANSIBLE_VAULT_PASSWORD > $ANSIBLE_VAULT_PASSWORD_FILE  # Save the Ansible Vault password to the file
    - echo "password" > secrets.yml  # Create a secrets file (example content)
    - ansible-vault encrypt secrets.yml --vault-password-file ~/.vault_pass --encrypt-vault-id=default  # Encrypt the secrets file using Ansible Vault
    - ansible --version  # Display Ansible version (example command)
    - ansible-playbook -i localhost playbooks/gitlab_ioc.yml --vault-password-file "$ANSIBLE_VAULT_PASSWORD_FILE" --extra-vars "ci_project=$CI_PROJECT_PATH ci_registry=$CI_REGISTRY ci_registry_user=$CI_REGISTRY_USER ci_registry_password=$CI_REGISTRY_PASSWORD ioc_config_source_repo=$CI_REPOSITORY_URL ci_tag=$CI_COMMIT_REF_NAME" -vvvv  # Run the Ansible playbook with extra variables
  tags: 
    - docker  # Tag to identify which runners should execute this job
  rules:
    - changes:
        - ioc_config.yml  # Only run this job if ioc_config.yml has changed
